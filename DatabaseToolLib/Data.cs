﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace CharmlingsDatabaseTool
{
    public static class Data
    {
        public static Dictionary<string, int> dictSkills = new Dictionary<string, int>();
        public static Dictionary<string, int> dictMons = new Dictionary<string, int>();
        public static Dictionary<string, int> dictPerks = new Dictionary<string, int>();
        public static Dictionary<string, int> dictItems = new Dictionary<string, int>();

        public static Dictionary<string, string> dictDevSkills = new Dictionary<string, string>();

        public static bool dbLoaded = false;

        public static XmlDocument tempDB;
        public static string[] enumPaths = new string[4];

    }
}
