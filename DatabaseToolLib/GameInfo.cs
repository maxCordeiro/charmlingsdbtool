﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CharmlingsDatabaseTool
{
    public partial class GameInfo
    {
        public enum MonsterFamily
        {
            AQUATIC,
            AERIAL,
            FAIRY,
            MYSTERIOUS,
            UNDERGROUND,
            NATURE,
            BEAST,
            MACHINE,
            DARK,
            FIELD,
            ORIGIN
        }
        public enum MonsterType
        {
            WATER,
            PLANT,
            FIRE,
            COSMIC,
            ELECTRIC,
            WIND,
            SHADOW,
            TECH,
            EARTH,
            NORMAL,
            NONE
        }
        public enum Rarity
        {
            NONE,
            COMMON,
            UNCOMMON,
            RARE,
            ULTRARARE,
            SPECIAL
        }

        public enum SkillType
        {
            SPECIAL,
            STRENGTH,
            MAGIC
        }

        public enum SkillPriority
        {
            NONE,
            BEFORE,
            REGULAR
        }

        public enum ExpType
        {
            LIGHT,
            STANDARD,
            TOUGH
        }
    }


}
