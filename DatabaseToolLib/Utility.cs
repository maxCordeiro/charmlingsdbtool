﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Xml;

namespace CharmlingsDatabaseTool
{
    public class Utility
    {
        string[] enumLists = new string[] { "Skills", "Monsters", "Perks", "Items" };

        public void LoadEnumsToDict(string gamePath)
        {
            for (int i = 0; i < enumLists.Length; i++)
            {
                using (StreamReader reader = new StreamReader(Data.enumPaths[i]))
                {
                    Dictionary<string, int> dictRef = new Dictionary<string, int>();

                    switch (i)
                    {
                        case 0:
                            dictRef = Data.dictSkills;
                            break;
                        case 1:
                            dictRef = Data.dictMons;
                            break;
                        case 2:
                            dictRef = Data.dictPerks;
                            break;
                        case 3:
                            dictRef = Data.dictItems;
                            break;
                    }

                    dictRef.Clear();

                    string line;
                    while ((line = reader.ReadLine()) != null)
                    {
                        string[] lineValues = line.Split('=');
                        
                        if (lineValues.Length == 2 && i < 4)
                        {
                            string tempKey = Regex.Match(lineValues[0], "[A-Z_]+").ToString();
                            int tempVal = int.Parse(Regex.Match(lineValues[1], "\\d+").ToString());

                            //System.Diagnostics.Debug.WriteLine(tempKey + ", " + tempVal);

                            dictRef.Add(tempKey, tempVal);
                        }
                    }
                }

                if (!File.Exists(gamePath + "/Assets/Scripts/Enums/notes_" + enumLists[i] + ".txt")) continue;

                using (StreamReader reader = new StreamReader(gamePath + "/Assets/Scripts/Enums/notes_" + enumLists[i] + ".txt"))
                {
                    Dictionary<string, string> dictRef = new Dictionary<string, string>();

                    switch (i)
                    {
                        case 0:
                            dictRef = Data.dictDevSkills;
                            break;
                    }

                    dictRef.Clear();

                    string line;
                    while ((line = reader.ReadLine()) != null)
                    {
                        string[] lineValues = line.Split('=');

                        if (lineValues.Length == 2)
                        {
                            dictRef.Add(lineValues[0], lineValues[1]);
                        }
                    }
                }
            }
        }

        public void WriteEnum(int index)
        {
            using (StreamWriter sw = File.CreateText(Data.enumPaths[index]))
            {
                sw.WriteLine("namespace GameDatabase");
                sw.WriteLine("{");
                sw.WriteLine("\tpublic enum " + enumLists[index] + " {");

                foreach (KeyValuePair<string, int> skill in Data.dictSkills)
                {
                    sw.WriteLine("\t\t" + skill.Key + " = " + skill.Value.ToString() + ",");
                }

                sw.WriteLine("\t}");
                sw.WriteLine("}");
            }           
        }

        public void WriteNotesToText(string path, int index)
        {
            using (StreamWriter file = new StreamWriter(path + "/Assets/Scripts/Enums/notes_" + enumLists[index] + ".txt"))
            {
                Dictionary<string, string> dictRef = new Dictionary<string, string>();

                switch (index)
                {
                    case 0:
                        dictRef = Data.dictDevSkills;
                        break;
                }

                foreach (var entry in dictRef)
                {
                    file.WriteLine("{0}={1}", entry.Key, entry.Value);
                }
            }
        }

        public void WriteFilesToGame(string backupPath, string dbPath)
        {
            string gamePath = Directory.GetParent(Directory.GetParent(dbPath).ToString()).ToString();
            string enumPath = gamePath + "/Assets/Scripts/Enums/";

            File.Copy(Path.Combine(backupPath + "db.xml"), dbPath);
            
            for (int i = 0; i < Data.enumPaths.Length; i++)
            {
                File.Copy(Path.Combine(Data.enumPaths[i]), enumPath);
            }
        }
    }
}
