﻿using System;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using System.Xml;

namespace CharmlingsDatabaseTool
{
    public partial class CharmlingsForm : Form
    {
        formMain form;

        XmlDocument db = Data.tempDB;
        XmlNodeList dblist;
        XmlElement currentElement;

        string currentName;
        int currentID;
        int selectedDropsTab = 0;
        int lastIndex = 0;

        NumericUpDown[] baseStats;
        TextBox[] maxStats;

        public CharmlingsForm(formMain _form)
        {
            InitializeComponent();
            form = _form;

            maxStats = new TextBox[6];
            maxStats[0] = textMaxStr;
            maxStats[1] = textMaxDef;
            maxStats[2] = textMaxMag;
            maxStats[3] = textMaxRes;
            maxStats[4] = textMaxAgi;
            maxStats[5] = textMaxHP;

            baseStats = new NumericUpDown[6];
            baseStats[0] = numBaseStr;
            baseStats[1] = numBaseDef;
            baseStats[2] = numBaseMag;
            baseStats[3] = numBaseRes;
            baseStats[4] = numBaseAgi;
            baseStats[5] = numBaseHP;
        }

        private void CharmlingsForm_Load(object sender, EventArgs e)
        {
            comboAttr.DataSource = Enum.GetValues(typeof(GameInfo.MonsterType));
            comboSecAttr.DataSource = Enum.GetValues(typeof(GameInfo.MonsterType));
            comboSecAttr.SelectedItem = Enum.Parse(typeof(GameInfo.MonsterType), "NONE");
        
            comboFamily.DataSource = Enum.GetValues(typeof(GameInfo.MonsterFamily));
            comboExp.DataSource = Enum.GetValues(typeof(GameInfo.ExpType));

            (dataGridCommon.Columns[0] as DataGridViewComboBoxColumn).Items.AddRange(Data.dictItems.Keys.ToArray());// = new BindingSource(Data.dictItems.Keys.ToArray(), null);
            (dataGridCommon.Columns[0] as DataGridViewComboBoxColumn).ValueType = typeof(string);
            dataGridCommon.Columns[0].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            
            (dataGridUncommon.Columns[0] as DataGridViewComboBoxColumn).Items.AddRange(Data.dictItems.Keys.ToArray());// = new BindingSource(Data.dictItems.Keys.ToArray(), null);
            (dataGridUncommon.Columns[0] as DataGridViewComboBoxColumn).ValueType = typeof(string);
            dataGridUncommon.Columns[0].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            
            (dataGridRare.Columns[0] as DataGridViewComboBoxColumn).Items.AddRange(Data.dictItems.Keys.ToArray());// = new BindingSource(Data.dictItems.Keys.ToArray(), null);
            (dataGridRare.Columns[0] as DataGridViewComboBoxColumn).ValueType = typeof(string);
            dataGridRare.Columns[0].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            
            comboPerk.DataSource = new BindingSource(Data.dictPerks.Keys.ToArray(), null);

            XmlNode root = db.DocumentElement;
            XmlNode monsterElement = root.SelectSingleNode("Monsters");
            dblist = monsterElement.ChildNodes;

            UpdateStatValues();

            UpdateGrid();
        }

        void UpdateGrid()
        {
            dataGridCharmlings.Rows.Clear();

            foreach (XmlNode node in dblist)
            {
                XmlElement element = (XmlElement)node;
                
                var imgarray = new Image[4];
                try
                {
                    var img = Image.FromFile(Properties.Settings.Default.GamePath + "/Assets/Resources/MonsterIcons/" + element.GetAttribute("id") + "_0.png");
                    for (int i = 0; i < 4; i++)
                    {
                        var index = i;
                        imgarray[index] = new Bitmap(28, 28);
                        var graphics = Graphics.FromImage(imgarray[index]);
                        graphics.DrawImage(img, new Rectangle(0, 0, 28, 28), new Rectangle(i * 28, 0, 28, 28), GraphicsUnit.Pixel);
                        graphics.Dispose();
                    }
                }
                catch { }

                dataGridCharmlings.Rows.Add(imgarray[0] == null? new Bitmap(1,1) : imgarray[0], Data.dictMons[element.GetAttribute("id")], element.GetAttribute("id"));
            }
        }

        private void CharmlingsForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            Properties.Settings.Default.inEditor = false;
            form.Enabled = true;
        }

        private void buttonAddMon_Click(object sender, EventArgs e)
        {
            dataGridCharmlings.Rows.Add();
        }

        private void dataGridView1_SelectionChanged(object sender, EventArgs e)
        {
            buttonRemMon.Enabled = dataGridCharmlings.SelectedCells.Count > 0;            
        }

        private void buttonRemMon_Click(object sender, EventArgs e)
        {
            var confirmResult = MessageBox.Show("Are you sure you want to delete this Charmling?", "", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);

            if (confirmResult == DialogResult.Yes)
            {
                dataGridCharmlings.Rows.RemoveAt(dataGridCharmlings.SelectedRows[0].Index);
            }
        }

        private void tabsDrops_TabIndexChanged(object sender, EventArgs e)
        {
            selectedDropsTab = tabsDrops.SelectedIndex;
        }

        public void UpdateDrops()
        {

        }

        void LoadData(XmlElement _element)
        {
            lastIndex = dataGridCharmlings.CurrentCell.RowIndex;

            textboxName.Text = _element.GetAttribute("id");
            numericID.Value = Data.dictMons[_element.GetAttribute("id")];

            currentName = charmlingsName.HeaderText;
            currentID = (int)numericID.Value;

            comboAttr.SelectedItem = Enum.Parse(typeof(GameInfo.MonsterType), _element.SelectSingleNode("firstattr").InnerText);
            comboSecAttr.SelectedItem = Enum.Parse(typeof(GameInfo.MonsterType), _element.SelectSingleNode("secattr").InnerText);

            comboFamily.SelectedItem = Enum.Parse(typeof(GameInfo.MonsterFamily), _element.SelectSingleNode("family").InnerText);
            comboExp.SelectedItem = Enum.Parse(typeof(GameInfo.ExpType), _element.SelectSingleNode("exp").InnerText);

            numWeight.Value = decimal.Parse(_element.SelectSingleNode("weight").InnerText);
            numHeight.Value = decimal.Parse(_element.SelectSingleNode("height").InnerText);
            checkPerk.Checked = bool.Parse(_element.SelectSingleNode("hasperk").InnerText);

            if (checkPerk.Checked)
            {
                comboPerk.Enabled = true;
                comboPerk.SelectedItem = _element.SelectSingleNode("perk").InnerText;
                
            } else
            {
                comboPerk.SelectedIndex = 0;
                comboPerk.Enabled = false;
            }

            XmlNodeList statNodes = _element.SelectSingleNode("basestats").SelectSingleNode("baseArray").ChildNodes;
            
            for (int i = 0; i < baseStats.Length; i++)
            {
                baseStats[i].Value = int.Parse(statNodes[i].InnerText);    
            }

            UpdateTotal();

            UpdateDropGrid(0);
            UpdateDropGrid(1);
            UpdateDropGrid(2);

            UpdateStatValues();
        }

        private void dataGridCharmlings_CurrentCellChanged(object sender, EventArgs e)
        {
            foreach (XmlNode node in dblist)
            {
                currentElement = (XmlElement)node;

                if (currentElement.GetAttribute("id") == dataGridCharmlings.CurrentRow.Cells[2].Value.ToString())
                {
                    LoadData(currentElement);
                    break;
                }
            }
        }

        private void numStatLevel_ValueChanged(object sender, EventArgs e)
        {
            UpdateStatValues();
        }

        void UpdateStatValues()
        {
            for (int i = 0; i < baseStats.Length; i++)
            {
                maxStats[i].Text = Math.Floor(((2.82f * ((float)baseStats[i].Value * (float)numStatLevel.Value * (1 + (0 * 0.0062f))) + 5.5f) / 100 + 2.5f)).ToString();
            }
        }

        void UpdateTotal()
        {
            int tempTotal = 0;
            for (int i = 0; i < baseStats.Length; i++)
            {
                tempTotal += (int)baseStats[i].Value;
            }

            labelTotal.Text = string.Format("Total: {0}", tempTotal);
        }


        private void numBaseStr_ValueChanged(object sender, EventArgs e)
        {
            UpdateTotal();
        }

        private void numBaseDef_ValueChanged(object sender, EventArgs e)
        {
            UpdateTotal();
        }

        private void numBaseMag_ValueChanged(object sender, EventArgs e)
        {
            UpdateTotal();
        }

        private void numBaseRes_ValueChanged(object sender, EventArgs e)
        {
            UpdateTotal();
        }

        private void numBaseAgi_ValueChanged(object sender, EventArgs e)
        {
            UpdateTotal();
        }

        private void numBaseHP_ValueChanged(object sender, EventArgs e)
        {
            UpdateTotal();
        }

        private void buttonSave_Click(object sender, EventArgs e)
        {
            SaveData(currentElement);

        }

        void SaveData(XmlElement _element)
        {
            if (currentName != textboxName.Text || currentID != (int)numericID.Value)
            {
                Data.dictMons.Remove(currentName);
                Data.dictMons.Add(textboxName.Text, (int)numericID.Value);
            }

            _element.SetAttribute("id", textboxName.Text);

            XmlNodeList statNodes = _element.SelectSingleNode("basestats").SelectSingleNode("baseArray").ChildNodes;

            for (int i = 0; i < statNodes.Count; i++)
            {
                statNodes[i].InnerText = baseStats[i].Value.ToString();
            }

            _element.SelectSingleNode("family").InnerText = comboFamily.SelectedItem.ToString();
            _element.SelectSingleNode("height").InnerText = numHeight.Value.ToString();
            _element.SelectSingleNode("weight").InnerText = numWeight.Value.ToString();
            _element.SelectSingleNode("firstattr").InnerText = comboAttr.SelectedItem.ToString();
            _element.SelectSingleNode("secattr").InnerText = comboSecAttr.SelectedItem.ToString();
            _element.SelectSingleNode("exp").InnerText = comboExp.SelectedItem.ToString();

            _element.SelectSingleNode("hasperk").InnerText = checkPerk.Checked.ToString();
            
            if (checkPerk.Checked)
            {
                _element.SelectSingleNode("perk").InnerText = comboPerk.SelectedItem.ToString();
            }
            

        }


        private void tabsDrops_SelectedIndexChanged(object sender, EventArgs e)
        {
            //UpdateDropGrid(tabsDrops.SelectedIndex);
        }

        void UpdateDropGrid(int tab)
        {
            XmlNodeList drops = currentElement.SelectNodes(dropNode(tab) + "drops");

            GetDropGrid(tab).Rows.Clear();
            GetDropCheck(tab).Checked = currentElement.SelectSingleNode("has" + dropNode(tab)).InnerText == "true";
            GetDropAddButton(tab).Enabled = GetDropCheck(tab).Checked;
            GetDropRemoveButton(tab).Enabled = GetDropCheck(tab).Checked;

            if (drops.Count == 0) return;

            Console.WriteLine(drops[0].InnerText);

            for (int i = 0; i < drops.Count; i++)
            {
                GetDropGrid(tab).Rows.Add(drops[0].InnerText);
            }

        }

        string dropNode(int index)
        {
            string dropNode = "";

            switch (index)
            {
                case 0:
                    dropNode = "common";
                    break;
                case 1:
                    dropNode = "uncommon";
                    break;
                case 2:
                    dropNode = "rare";
                    break;
                default:
                    break;
            }

            return dropNode;
        }

        DataGridView GetDropGrid(int index)
        {
            switch (index)
            {
                case 0:
                    return dataGridCommon;
                case 1:
                    return dataGridUncommon;
                case 2:
                    return dataGridRare;
                default:
                    return null;
            }
        }

        CheckBox GetDropCheck(int index)
        {
            switch (index)
            {
                case 0:
                    return checkDropsComm;
                case 1:
                    return checkDropsUncomm;
                case 2:
                    return checkDropsRare;
                default:
                    return null;
            }
        }

        Button GetDropAddButton(int index)
        {
            switch (index)
            {
                case 0:
                    return buttonAddComm;
                case 1:
                    return buttonAddUncomm;
                case 2:
                    return buttonAddRare;
                default:
                    return null;
            }
        }

        Button GetDropRemoveButton(int index)
        {
            switch (index)
            {
                case 0:
                    return buttonRemComm;
                case 1:
                    return buttonRemUncomm;
                case 2:
                    return buttonRemRare;
                default:
                    return null;
            }
        }
        
        private void checkDrops_Click(object sender, EventArgs e)
        {
            EnableDrops();
        }

        private void buttonAddComm_Click(object sender, EventArgs e)
        {
            if (!checkDropsComm.Checked) return;

            GetDropGrid(tabsDrops.SelectedIndex).Rows.Add("POTION");
        }

        private void buttonAddUncomm_Click(object sender, EventArgs e)
        {
            if (!checkDropsUncomm.Checked) return;

            GetDropGrid(tabsDrops.SelectedIndex).Rows.Add("POTION");
        }

        private void buttonAddRare_Click(object sender, EventArgs e)
        {
            if (!checkDropsRare.Checked) return;

            GetDropGrid(tabsDrops.SelectedIndex).Rows.Add("POTION");
        }
                
        private void buttonRemComm_Click(object sender, EventArgs e)
        {
            GetDropGrid(tabsDrops.SelectedIndex).Rows.RemoveAt(GetDropGrid(tabsDrops.SelectedIndex).SelectedRows[0].Index);
        }

        private void buttonRemUncomm_Click(object sender, EventArgs e)
        {
            GetDropGrid(tabsDrops.SelectedIndex).Rows.RemoveAt(GetDropGrid(tabsDrops.SelectedIndex).SelectedRows[0].Index);
        }

        private void buttonRemRare_Click(object sender, EventArgs e)
        {
            GetDropGrid(tabsDrops.SelectedIndex).Rows.RemoveAt(GetDropGrid(tabsDrops.SelectedIndex).SelectedRows[0].Index);
        }

        private void checkDropsUncomm_CheckedChanged(object sender, EventArgs e)
        {
        }

        private void checkDropsRare_CheckedChanged(object sender, EventArgs e)
        {
        }

        void EnableDrops()
        {
            if (GetDropGrid(tabsDrops.SelectedIndex).Rows.Count > 0 && GetDropCheck(tabsDrops.SelectedIndex).Checked)
            {
                var confirmResult = MessageBox.Show("Unchecking will clear existing drops. Continue?", "", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);

                if (confirmResult == DialogResult.Yes)
                {
                    GetDropGrid(tabsDrops.SelectedIndex).Rows.Clear();
                } else { return; }
            }

            GetDropCheck(tabsDrops.SelectedIndex).Checked = !GetDropCheck(tabsDrops.SelectedIndex).Checked;

            GetDropAddButton(tabsDrops.SelectedIndex).Enabled = GetDropCheck(tabsDrops.SelectedIndex).Checked;
            GetDropRemoveButton(tabsDrops.SelectedIndex).Enabled = GetDropCheck(tabsDrops.SelectedIndex).Checked;
        }

        private void checkDropsUncomm_Click(object sender, EventArgs e)
        {
            EnableDrops();
        }

        private void checkDropsRare_Click(object sender, EventArgs e)
        {
            EnableDrops();
        }
    }
}
