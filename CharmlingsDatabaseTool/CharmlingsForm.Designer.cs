﻿namespace CharmlingsDatabaseTool
{
    partial class CharmlingsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataGridCharmlings = new System.Windows.Forms.DataGridView();
            this.charmlingsIcon = new System.Windows.Forms.DataGridViewImageColumn();
            this.charmlingsID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.charmlingsName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.buttonAddMon = new System.Windows.Forms.Button();
            this.buttonRemMon = new System.Windows.Forms.Button();
            this.menuMain = new System.Windows.Forms.MenuStrip();
            this.editToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.viewToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.labelName = new System.Windows.Forms.Label();
            this.labelID = new System.Windows.Forms.Label();
            this.textboxName = new System.Windows.Forms.TextBox();
            this.numericID = new System.Windows.Forms.NumericUpDown();
            this.labelAttribute = new System.Windows.Forms.Label();
            this.comboAttr = new System.Windows.Forms.ComboBox();
            this.labelSecAttr = new System.Windows.Forms.Label();
            this.comboSecAttr = new System.Windows.Forms.ComboBox();
            this.labelFamily = new System.Windows.Forms.Label();
            this.comboFamily = new System.Windows.Forms.ComboBox();
            this.labelExp = new System.Windows.Forms.Label();
            this.comboExp = new System.Windows.Forms.ComboBox();
            this.numSecID = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.groupStats = new System.Windows.Forms.GroupBox();
            this.textMaxHP = new System.Windows.Forms.TextBox();
            this.textMaxAgi = new System.Windows.Forms.TextBox();
            this.textMaxRes = new System.Windows.Forms.TextBox();
            this.textMaxMag = new System.Windows.Forms.TextBox();
            this.textMaxDef = new System.Windows.Forms.TextBox();
            this.textMaxStr = new System.Windows.Forms.TextBox();
            this.labelTotal = new System.Windows.Forms.Label();
            this.numStatLevel = new System.Windows.Forms.NumericUpDown();
            this.labelBaseHP = new System.Windows.Forms.Label();
            this.numBaseHP = new System.Windows.Forms.NumericUpDown();
            this.labelBaseRes = new System.Windows.Forms.Label();
            this.numBaseAgi = new System.Windows.Forms.NumericUpDown();
            this.numBaseRes = new System.Windows.Forms.NumericUpDown();
            this.numBaseMag = new System.Windows.Forms.NumericUpDown();
            this.numBaseDef = new System.Windows.Forms.NumericUpDown();
            this.labelBaseAgi = new System.Windows.Forms.Label();
            this.labelBaseMag = new System.Windows.Forms.Label();
            this.labelBaseDef = new System.Windows.Forms.Label();
            this.numBaseStr = new System.Windows.Forms.NumericUpDown();
            this.labelBaseStr = new System.Windows.Forms.Label();
            this.checkPerk = new System.Windows.Forms.CheckBox();
            this.comboPerk = new System.Windows.Forms.ComboBox();
            this.tabsDrops = new System.Windows.Forms.TabControl();
            this.tabCommon = new System.Windows.Forms.TabPage();
            this.dataGridCommon = new System.Windows.Forms.DataGridView();
            this.columnDrops = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.buttonAddComm = new System.Windows.Forms.Button();
            this.checkDropsComm = new System.Windows.Forms.CheckBox();
            this.buttonRemComm = new System.Windows.Forms.Button();
            this.tabUncommon = new System.Windows.Forms.TabPage();
            this.buttonAddUncomm = new System.Windows.Forms.Button();
            this.checkDropsUncomm = new System.Windows.Forms.CheckBox();
            this.buttonRemUncomm = new System.Windows.Forms.Button();
            this.dataGridUncommon = new System.Windows.Forms.DataGridView();
            this.dataGridViewComboBoxColumn1 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.tabRare = new System.Windows.Forms.TabPage();
            this.buttonAddRare = new System.Windows.Forms.Button();
            this.checkDropsRare = new System.Windows.Forms.CheckBox();
            this.buttonRemRare = new System.Windows.Forms.Button();
            this.dataGridRare = new System.Windows.Forms.DataGridView();
            this.dataGridViewComboBoxColumn2 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.buttonSave = new System.Windows.Forms.Button();
            this.numWeight = new System.Windows.Forms.NumericUpDown();
            this.labelWeight = new System.Windows.Forms.Label();
            this.labelHeight = new System.Windows.Forms.Label();
            this.numHeight = new System.Windows.Forms.NumericUpDown();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridCharmlings)).BeginInit();
            this.menuMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numSecID)).BeginInit();
            this.groupStats.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numStatLevel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numBaseHP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numBaseAgi)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numBaseRes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numBaseMag)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numBaseDef)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numBaseStr)).BeginInit();
            this.tabsDrops.SuspendLayout();
            this.tabCommon.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridCommon)).BeginInit();
            this.tabUncommon.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridUncommon)).BeginInit();
            this.tabRare.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridRare)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numWeight)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numHeight)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridCharmlings
            // 
            this.dataGridCharmlings.AllowUserToAddRows = false;
            this.dataGridCharmlings.AllowUserToDeleteRows = false;
            this.dataGridCharmlings.AllowUserToResizeColumns = false;
            this.dataGridCharmlings.AllowUserToResizeRows = false;
            this.dataGridCharmlings.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridCharmlings.ColumnHeadersVisible = false;
            this.dataGridCharmlings.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.charmlingsIcon,
            this.charmlingsID,
            this.charmlingsName});
            this.dataGridCharmlings.Location = new System.Drawing.Point(12, 27);
            this.dataGridCharmlings.MultiSelect = false;
            this.dataGridCharmlings.Name = "dataGridCharmlings";
            this.dataGridCharmlings.ReadOnly = true;
            this.dataGridCharmlings.RowHeadersVisible = false;
            this.dataGridCharmlings.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.AutoSizeToAllHeaders;
            this.dataGridCharmlings.RowTemplate.Height = 28;
            this.dataGridCharmlings.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridCharmlings.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridCharmlings.Size = new System.Drawing.Size(314, 472);
            this.dataGridCharmlings.TabIndex = 0;
            this.dataGridCharmlings.CurrentCellChanged += new System.EventHandler(this.dataGridCharmlings_CurrentCellChanged);
            this.dataGridCharmlings.SelectionChanged += new System.EventHandler(this.dataGridView1_SelectionChanged);
            // 
            // charmlingsIcon
            // 
            this.charmlingsIcon.HeaderText = "";
            this.charmlingsIcon.MinimumWidth = 28;
            this.charmlingsIcon.Name = "charmlingsIcon";
            this.charmlingsIcon.ReadOnly = true;
            this.charmlingsIcon.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.charmlingsIcon.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.charmlingsIcon.Width = 30;
            // 
            // charmlingsID
            // 
            this.charmlingsID.HeaderText = "ID";
            this.charmlingsID.MinimumWidth = 32;
            this.charmlingsID.Name = "charmlingsID";
            this.charmlingsID.ReadOnly = true;
            this.charmlingsID.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.charmlingsID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.charmlingsID.Width = 40;
            // 
            // charmlingsName
            // 
            this.charmlingsName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.charmlingsName.HeaderText = "Charmling";
            this.charmlingsName.Name = "charmlingsName";
            this.charmlingsName.ReadOnly = true;
            this.charmlingsName.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // buttonAddMon
            // 
            this.buttonAddMon.Location = new System.Drawing.Point(12, 505);
            this.buttonAddMon.Name = "buttonAddMon";
            this.buttonAddMon.Size = new System.Drawing.Size(232, 30);
            this.buttonAddMon.TabIndex = 1;
            this.buttonAddMon.Text = "Add";
            this.buttonAddMon.UseVisualStyleBackColor = true;
            this.buttonAddMon.Click += new System.EventHandler(this.buttonAddMon_Click);
            // 
            // buttonRemMon
            // 
            this.buttonRemMon.Location = new System.Drawing.Point(250, 505);
            this.buttonRemMon.Name = "buttonRemMon";
            this.buttonRemMon.Size = new System.Drawing.Size(76, 30);
            this.buttonRemMon.TabIndex = 1;
            this.buttonRemMon.Text = "Remove";
            this.buttonRemMon.UseVisualStyleBackColor = true;
            this.buttonRemMon.Click += new System.EventHandler(this.buttonRemMon_Click);
            // 
            // menuMain
            // 
            this.menuMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.editToolStripMenuItem,
            this.viewToolStripMenuItem});
            this.menuMain.Location = new System.Drawing.Point(0, 0);
            this.menuMain.Name = "menuMain";
            this.menuMain.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.menuMain.Size = new System.Drawing.Size(676, 24);
            this.menuMain.TabIndex = 2;
            this.menuMain.Text = "menuStrip1";
            // 
            // editToolStripMenuItem
            // 
            this.editToolStripMenuItem.Name = "editToolStripMenuItem";
            this.editToolStripMenuItem.Size = new System.Drawing.Size(39, 20);
            this.editToolStripMenuItem.Text = "Edit";
            // 
            // viewToolStripMenuItem
            // 
            this.viewToolStripMenuItem.Name = "viewToolStripMenuItem";
            this.viewToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.viewToolStripMenuItem.Text = "View";
            // 
            // labelName
            // 
            this.labelName.AutoSize = true;
            this.labelName.Location = new System.Drawing.Point(332, 27);
            this.labelName.Name = "labelName";
            this.labelName.Size = new System.Drawing.Size(84, 13);
            this.labelName.TabIndex = 3;
            this.labelName.Text = "Charmling Name";
            // 
            // labelID
            // 
            this.labelID.AutoSize = true;
            this.labelID.Location = new System.Drawing.Point(551, 27);
            this.labelID.Name = "labelID";
            this.labelID.Size = new System.Drawing.Size(18, 13);
            this.labelID.TabIndex = 4;
            this.labelID.Text = "ID";
            // 
            // textboxName
            // 
            this.textboxName.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textboxName.Location = new System.Drawing.Point(335, 43);
            this.textboxName.Name = "textboxName";
            this.textboxName.Size = new System.Drawing.Size(213, 20);
            this.textboxName.TabIndex = 5;
            // 
            // numericID
            // 
            this.numericID.Location = new System.Drawing.Point(554, 43);
            this.numericID.Maximum = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.numericID.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericID.Name = "numericID";
            this.numericID.Size = new System.Drawing.Size(52, 20);
            this.numericID.TabIndex = 29;
            this.numericID.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // labelAttribute
            // 
            this.labelAttribute.AutoSize = true;
            this.labelAttribute.Location = new System.Drawing.Point(332, 69);
            this.labelAttribute.Name = "labelAttribute";
            this.labelAttribute.Size = new System.Drawing.Size(46, 13);
            this.labelAttribute.TabIndex = 31;
            this.labelAttribute.Text = "Attribute";
            // 
            // comboAttr
            // 
            this.comboAttr.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboAttr.FormattingEnabled = true;
            this.comboAttr.Location = new System.Drawing.Point(335, 85);
            this.comboAttr.Name = "comboAttr";
            this.comboAttr.Size = new System.Drawing.Size(132, 21);
            this.comboAttr.TabIndex = 30;
            // 
            // labelSecAttr
            // 
            this.labelSecAttr.AutoSize = true;
            this.labelSecAttr.Location = new System.Drawing.Point(332, 109);
            this.labelSecAttr.Name = "labelSecAttr";
            this.labelSecAttr.Size = new System.Drawing.Size(86, 13);
            this.labelSecAttr.TabIndex = 33;
            this.labelSecAttr.Text = "Second Attribute";
            // 
            // comboSecAttr
            // 
            this.comboSecAttr.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboSecAttr.FormattingEnabled = true;
            this.comboSecAttr.Location = new System.Drawing.Point(335, 125);
            this.comboSecAttr.Name = "comboSecAttr";
            this.comboSecAttr.Size = new System.Drawing.Size(132, 21);
            this.comboSecAttr.TabIndex = 32;
            // 
            // labelFamily
            // 
            this.labelFamily.AutoSize = true;
            this.labelFamily.Location = new System.Drawing.Point(332, 149);
            this.labelFamily.Name = "labelFamily";
            this.labelFamily.Size = new System.Drawing.Size(36, 13);
            this.labelFamily.TabIndex = 35;
            this.labelFamily.Text = "Family";
            // 
            // comboFamily
            // 
            this.comboFamily.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboFamily.FormattingEnabled = true;
            this.comboFamily.Location = new System.Drawing.Point(335, 165);
            this.comboFamily.Name = "comboFamily";
            this.comboFamily.Size = new System.Drawing.Size(132, 21);
            this.comboFamily.TabIndex = 34;
            // 
            // labelExp
            // 
            this.labelExp.AutoSize = true;
            this.labelExp.Location = new System.Drawing.Point(332, 189);
            this.labelExp.Name = "labelExp";
            this.labelExp.Size = new System.Drawing.Size(65, 13);
            this.labelExp.TabIndex = 37;
            this.labelExp.Text = "Exp. Growth";
            // 
            // comboExp
            // 
            this.comboExp.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboExp.FormattingEnabled = true;
            this.comboExp.Location = new System.Drawing.Point(335, 205);
            this.comboExp.Name = "comboExp";
            this.comboExp.Size = new System.Drawing.Size(132, 21);
            this.comboExp.TabIndex = 36;
            // 
            // numSecID
            // 
            this.numSecID.Location = new System.Drawing.Point(612, 43);
            this.numSecID.Maximum = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.numSecID.Name = "numSecID";
            this.numSecID.Size = new System.Drawing.Size(52, 20);
            this.numSecID.TabIndex = 39;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(609, 27);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(43, 13);
            this.label1.TabIndex = 38;
            this.label1.Text = "Sec. ID";
            // 
            // groupStats
            // 
            this.groupStats.Controls.Add(this.textMaxHP);
            this.groupStats.Controls.Add(this.textMaxAgi);
            this.groupStats.Controls.Add(this.textMaxRes);
            this.groupStats.Controls.Add(this.textMaxMag);
            this.groupStats.Controls.Add(this.textMaxDef);
            this.groupStats.Controls.Add(this.textMaxStr);
            this.groupStats.Controls.Add(this.labelTotal);
            this.groupStats.Controls.Add(this.numStatLevel);
            this.groupStats.Controls.Add(this.labelBaseHP);
            this.groupStats.Controls.Add(this.numBaseHP);
            this.groupStats.Controls.Add(this.labelBaseRes);
            this.groupStats.Controls.Add(this.numBaseAgi);
            this.groupStats.Controls.Add(this.numBaseRes);
            this.groupStats.Controls.Add(this.numBaseMag);
            this.groupStats.Controls.Add(this.numBaseDef);
            this.groupStats.Controls.Add(this.labelBaseAgi);
            this.groupStats.Controls.Add(this.labelBaseMag);
            this.groupStats.Controls.Add(this.labelBaseDef);
            this.groupStats.Controls.Add(this.numBaseStr);
            this.groupStats.Controls.Add(this.labelBaseStr);
            this.groupStats.Location = new System.Drawing.Point(473, 69);
            this.groupStats.Name = "groupStats";
            this.groupStats.Size = new System.Drawing.Size(191, 205);
            this.groupStats.TabIndex = 40;
            this.groupStats.TabStop = false;
            this.groupStats.Text = "Base Stats";
            // 
            // textMaxHP
            // 
            this.textMaxHP.Enabled = false;
            this.textMaxHP.Location = new System.Drawing.Point(132, 146);
            this.textMaxHP.Name = "textMaxHP";
            this.textMaxHP.Size = new System.Drawing.Size(52, 20);
            this.textMaxHP.TabIndex = 43;
            // 
            // textMaxAgi
            // 
            this.textMaxAgi.Enabled = false;
            this.textMaxAgi.Location = new System.Drawing.Point(132, 119);
            this.textMaxAgi.Name = "textMaxAgi";
            this.textMaxAgi.Size = new System.Drawing.Size(52, 20);
            this.textMaxAgi.TabIndex = 43;
            // 
            // textMaxRes
            // 
            this.textMaxRes.Enabled = false;
            this.textMaxRes.Location = new System.Drawing.Point(132, 93);
            this.textMaxRes.Name = "textMaxRes";
            this.textMaxRes.Size = new System.Drawing.Size(52, 20);
            this.textMaxRes.TabIndex = 43;
            // 
            // textMaxMag
            // 
            this.textMaxMag.Enabled = false;
            this.textMaxMag.Location = new System.Drawing.Point(132, 67);
            this.textMaxMag.Name = "textMaxMag";
            this.textMaxMag.Size = new System.Drawing.Size(52, 20);
            this.textMaxMag.TabIndex = 43;
            // 
            // textMaxDef
            // 
            this.textMaxDef.Enabled = false;
            this.textMaxDef.Location = new System.Drawing.Point(132, 41);
            this.textMaxDef.Name = "textMaxDef";
            this.textMaxDef.Size = new System.Drawing.Size(52, 20);
            this.textMaxDef.TabIndex = 43;
            // 
            // textMaxStr
            // 
            this.textMaxStr.Enabled = false;
            this.textMaxStr.Location = new System.Drawing.Point(132, 16);
            this.textMaxStr.Name = "textMaxStr";
            this.textMaxStr.Size = new System.Drawing.Size(52, 20);
            this.textMaxStr.TabIndex = 42;
            // 
            // labelTotal
            // 
            this.labelTotal.AutoSize = true;
            this.labelTotal.Location = new System.Drawing.Point(6, 176);
            this.labelTotal.Name = "labelTotal";
            this.labelTotal.Size = new System.Drawing.Size(34, 13);
            this.labelTotal.TabIndex = 41;
            this.labelTotal.Text = "Total:";
            // 
            // numStatLevel
            // 
            this.numStatLevel.Location = new System.Drawing.Point(132, 174);
            this.numStatLevel.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numStatLevel.Name = "numStatLevel";
            this.numStatLevel.Size = new System.Drawing.Size(52, 20);
            this.numStatLevel.TabIndex = 12;
            this.numStatLevel.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numStatLevel.ValueChanged += new System.EventHandler(this.numStatLevel_ValueChanged);
            // 
            // labelBaseHP
            // 
            this.labelBaseHP.AutoSize = true;
            this.labelBaseHP.Location = new System.Drawing.Point(6, 148);
            this.labelBaseHP.Name = "labelBaseHP";
            this.labelBaseHP.Size = new System.Drawing.Size(22, 13);
            this.labelBaseHP.TabIndex = 6;
            this.labelBaseHP.Text = "HP";
            // 
            // numBaseHP
            // 
            this.numBaseHP.Location = new System.Drawing.Point(74, 146);
            this.numBaseHP.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.numBaseHP.Name = "numBaseHP";
            this.numBaseHP.Size = new System.Drawing.Size(52, 20);
            this.numBaseHP.TabIndex = 10;
            this.numBaseHP.ValueChanged += new System.EventHandler(this.numBaseHP_ValueChanged);
            // 
            // labelBaseRes
            // 
            this.labelBaseRes.AutoSize = true;
            this.labelBaseRes.Location = new System.Drawing.Point(6, 96);
            this.labelBaseRes.Name = "labelBaseRes";
            this.labelBaseRes.Size = new System.Drawing.Size(60, 13);
            this.labelBaseRes.TabIndex = 4;
            this.labelBaseRes.Text = "Resistance";
            // 
            // numBaseAgi
            // 
            this.numBaseAgi.Location = new System.Drawing.Point(74, 120);
            this.numBaseAgi.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.numBaseAgi.Name = "numBaseAgi";
            this.numBaseAgi.Size = new System.Drawing.Size(52, 20);
            this.numBaseAgi.TabIndex = 9;
            this.numBaseAgi.ValueChanged += new System.EventHandler(this.numBaseAgi_ValueChanged);
            // 
            // numBaseRes
            // 
            this.numBaseRes.Location = new System.Drawing.Point(74, 94);
            this.numBaseRes.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.numBaseRes.Name = "numBaseRes";
            this.numBaseRes.Size = new System.Drawing.Size(52, 20);
            this.numBaseRes.TabIndex = 8;
            this.numBaseRes.ValueChanged += new System.EventHandler(this.numBaseRes_ValueChanged);
            // 
            // numBaseMag
            // 
            this.numBaseMag.Location = new System.Drawing.Point(74, 68);
            this.numBaseMag.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.numBaseMag.Name = "numBaseMag";
            this.numBaseMag.Size = new System.Drawing.Size(52, 20);
            this.numBaseMag.TabIndex = 7;
            this.numBaseMag.ValueChanged += new System.EventHandler(this.numBaseMag_ValueChanged);
            // 
            // numBaseDef
            // 
            this.numBaseDef.Location = new System.Drawing.Point(74, 42);
            this.numBaseDef.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.numBaseDef.Name = "numBaseDef";
            this.numBaseDef.Size = new System.Drawing.Size(52, 20);
            this.numBaseDef.TabIndex = 6;
            this.numBaseDef.ValueChanged += new System.EventHandler(this.numBaseDef_ValueChanged);
            // 
            // labelBaseAgi
            // 
            this.labelBaseAgi.AutoSize = true;
            this.labelBaseAgi.Location = new System.Drawing.Point(6, 122);
            this.labelBaseAgi.Name = "labelBaseAgi";
            this.labelBaseAgi.Size = new System.Drawing.Size(34, 13);
            this.labelBaseAgi.TabIndex = 5;
            this.labelBaseAgi.Text = "Agility";
            // 
            // labelBaseMag
            // 
            this.labelBaseMag.AutoSize = true;
            this.labelBaseMag.Location = new System.Drawing.Point(6, 70);
            this.labelBaseMag.Name = "labelBaseMag";
            this.labelBaseMag.Size = new System.Drawing.Size(36, 13);
            this.labelBaseMag.TabIndex = 3;
            this.labelBaseMag.Text = "Magic";
            // 
            // labelBaseDef
            // 
            this.labelBaseDef.AutoSize = true;
            this.labelBaseDef.Location = new System.Drawing.Point(6, 44);
            this.labelBaseDef.Name = "labelBaseDef";
            this.labelBaseDef.Size = new System.Drawing.Size(47, 13);
            this.labelBaseDef.TabIndex = 2;
            this.labelBaseDef.Text = "Defense";
            // 
            // numBaseStr
            // 
            this.numBaseStr.Location = new System.Drawing.Point(74, 16);
            this.numBaseStr.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.numBaseStr.Name = "numBaseStr";
            this.numBaseStr.Size = new System.Drawing.Size(52, 20);
            this.numBaseStr.TabIndex = 1;
            this.numBaseStr.ValueChanged += new System.EventHandler(this.numBaseStr_ValueChanged);
            // 
            // labelBaseStr
            // 
            this.labelBaseStr.AutoSize = true;
            this.labelBaseStr.Location = new System.Drawing.Point(6, 18);
            this.labelBaseStr.Name = "labelBaseStr";
            this.labelBaseStr.Size = new System.Drawing.Size(47, 13);
            this.labelBaseStr.TabIndex = 0;
            this.labelBaseStr.Text = "Strength";
            // 
            // checkPerk
            // 
            this.checkPerk.AutoSize = true;
            this.checkPerk.Location = new System.Drawing.Point(335, 232);
            this.checkPerk.Name = "checkPerk";
            this.checkPerk.Size = new System.Drawing.Size(48, 17);
            this.checkPerk.TabIndex = 42;
            this.checkPerk.Text = "Perk";
            this.checkPerk.UseVisualStyleBackColor = true;
            // 
            // comboPerk
            // 
            this.comboPerk.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboPerk.FormattingEnabled = true;
            this.comboPerk.Location = new System.Drawing.Point(335, 252);
            this.comboPerk.Name = "comboPerk";
            this.comboPerk.Size = new System.Drawing.Size(132, 21);
            this.comboPerk.TabIndex = 41;
            // 
            // tabsDrops
            // 
            this.tabsDrops.Controls.Add(this.tabCommon);
            this.tabsDrops.Controls.Add(this.tabUncommon);
            this.tabsDrops.Controls.Add(this.tabRare);
            this.tabsDrops.Location = new System.Drawing.Point(335, 306);
            this.tabsDrops.Name = "tabsDrops";
            this.tabsDrops.SelectedIndex = 0;
            this.tabsDrops.Size = new System.Drawing.Size(340, 193);
            this.tabsDrops.TabIndex = 43;
            this.tabsDrops.SelectedIndexChanged += new System.EventHandler(this.tabsDrops_SelectedIndexChanged);
            // 
            // tabCommon
            // 
            this.tabCommon.Controls.Add(this.dataGridCommon);
            this.tabCommon.Controls.Add(this.buttonAddComm);
            this.tabCommon.Controls.Add(this.checkDropsComm);
            this.tabCommon.Controls.Add(this.buttonRemComm);
            this.tabCommon.Location = new System.Drawing.Point(4, 22);
            this.tabCommon.Name = "tabCommon";
            this.tabCommon.Padding = new System.Windows.Forms.Padding(3);
            this.tabCommon.Size = new System.Drawing.Size(332, 167);
            this.tabCommon.TabIndex = 0;
            this.tabCommon.Text = "Common";
            this.tabCommon.UseVisualStyleBackColor = true;
            // 
            // dataGridCommon
            // 
            this.dataGridCommon.AllowUserToAddRows = false;
            this.dataGridCommon.AllowUserToDeleteRows = false;
            this.dataGridCommon.AllowUserToResizeColumns = false;
            this.dataGridCommon.AllowUserToResizeRows = false;
            this.dataGridCommon.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridCommon.ColumnHeadersVisible = false;
            this.dataGridCommon.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.columnDrops});
            this.dataGridCommon.Location = new System.Drawing.Point(108, 6);
            this.dataGridCommon.Name = "dataGridCommon";
            this.dataGridCommon.RowHeadersWidth = 16;
            this.dataGridCommon.Size = new System.Drawing.Size(217, 155);
            this.dataGridCommon.TabIndex = 0;
            // 
            // columnDrops
            // 
            this.columnDrops.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.columnDrops.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.columnDrops.HeaderText = "Drops";
            this.columnDrops.Name = "columnDrops";
            // 
            // buttonAddComm
            // 
            this.buttonAddComm.Enabled = false;
            this.buttonAddComm.Location = new System.Drawing.Point(6, 109);
            this.buttonAddComm.Name = "buttonAddComm";
            this.buttonAddComm.Size = new System.Drawing.Size(96, 23);
            this.buttonAddComm.TabIndex = 1;
            this.buttonAddComm.Text = "Add";
            this.buttonAddComm.UseVisualStyleBackColor = true;
            this.buttonAddComm.Click += new System.EventHandler(this.buttonAddComm_Click);
            // 
            // checkDropsComm
            // 
            this.checkDropsComm.AutoCheck = false;
            this.checkDropsComm.AutoSize = true;
            this.checkDropsComm.Location = new System.Drawing.Point(6, 86);
            this.checkDropsComm.Name = "checkDropsComm";
            this.checkDropsComm.Size = new System.Drawing.Size(65, 17);
            this.checkDropsComm.TabIndex = 3;
            this.checkDropsComm.Text = "Enabled";
            this.checkDropsComm.UseVisualStyleBackColor = true;
            this.checkDropsComm.Click += new System.EventHandler(this.checkDrops_Click);
            // 
            // buttonRemComm
            // 
            this.buttonRemComm.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonRemComm.Location = new System.Drawing.Point(6, 138);
            this.buttonRemComm.Name = "buttonRemComm";
            this.buttonRemComm.Size = new System.Drawing.Size(96, 23);
            this.buttonRemComm.TabIndex = 2;
            this.buttonRemComm.Text = "Remove";
            this.buttonRemComm.UseVisualStyleBackColor = true;
            this.buttonRemComm.Click += new System.EventHandler(this.buttonRemComm_Click);
            // 
            // tabUncommon
            // 
            this.tabUncommon.Controls.Add(this.buttonAddUncomm);
            this.tabUncommon.Controls.Add(this.checkDropsUncomm);
            this.tabUncommon.Controls.Add(this.buttonRemUncomm);
            this.tabUncommon.Controls.Add(this.dataGridUncommon);
            this.tabUncommon.Location = new System.Drawing.Point(4, 22);
            this.tabUncommon.Name = "tabUncommon";
            this.tabUncommon.Padding = new System.Windows.Forms.Padding(3);
            this.tabUncommon.Size = new System.Drawing.Size(332, 167);
            this.tabUncommon.TabIndex = 1;
            this.tabUncommon.Text = "Uncommon";
            this.tabUncommon.UseVisualStyleBackColor = true;
            // 
            // buttonAddUncomm
            // 
            this.buttonAddUncomm.Enabled = false;
            this.buttonAddUncomm.Location = new System.Drawing.Point(6, 109);
            this.buttonAddUncomm.Name = "buttonAddUncomm";
            this.buttonAddUncomm.Size = new System.Drawing.Size(96, 23);
            this.buttonAddUncomm.TabIndex = 4;
            this.buttonAddUncomm.Text = "Add";
            this.buttonAddUncomm.UseVisualStyleBackColor = true;
            this.buttonAddUncomm.Click += new System.EventHandler(this.buttonAddUncomm_Click);
            // 
            // checkDropsUncomm
            // 
            this.checkDropsUncomm.AutoCheck = false;
            this.checkDropsUncomm.AutoSize = true;
            this.checkDropsUncomm.Location = new System.Drawing.Point(6, 86);
            this.checkDropsUncomm.Name = "checkDropsUncomm";
            this.checkDropsUncomm.Size = new System.Drawing.Size(65, 17);
            this.checkDropsUncomm.TabIndex = 6;
            this.checkDropsUncomm.Text = "Enabled";
            this.checkDropsUncomm.UseVisualStyleBackColor = true;
            this.checkDropsUncomm.CheckedChanged += new System.EventHandler(this.checkDropsUncomm_CheckedChanged);
            this.checkDropsUncomm.Click += new System.EventHandler(this.checkDropsUncomm_Click);
            // 
            // buttonRemUncomm
            // 
            this.buttonRemUncomm.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonRemUncomm.Location = new System.Drawing.Point(6, 138);
            this.buttonRemUncomm.Name = "buttonRemUncomm";
            this.buttonRemUncomm.Size = new System.Drawing.Size(96, 23);
            this.buttonRemUncomm.TabIndex = 5;
            this.buttonRemUncomm.Text = "Remove";
            this.buttonRemUncomm.UseVisualStyleBackColor = true;
            this.buttonRemUncomm.Click += new System.EventHandler(this.buttonRemUncomm_Click);
            // 
            // dataGridUncommon
            // 
            this.dataGridUncommon.AllowUserToAddRows = false;
            this.dataGridUncommon.AllowUserToDeleteRows = false;
            this.dataGridUncommon.AllowUserToResizeColumns = false;
            this.dataGridUncommon.AllowUserToResizeRows = false;
            this.dataGridUncommon.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridUncommon.ColumnHeadersVisible = false;
            this.dataGridUncommon.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewComboBoxColumn1});
            this.dataGridUncommon.Location = new System.Drawing.Point(108, 6);
            this.dataGridUncommon.Name = "dataGridUncommon";
            this.dataGridUncommon.RowHeadersWidth = 16;
            this.dataGridUncommon.Size = new System.Drawing.Size(217, 155);
            this.dataGridUncommon.TabIndex = 1;
            // 
            // dataGridViewComboBoxColumn1
            // 
            this.dataGridViewComboBoxColumn1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewComboBoxColumn1.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.dataGridViewComboBoxColumn1.HeaderText = "Drops";
            this.dataGridViewComboBoxColumn1.Name = "dataGridViewComboBoxColumn1";
            // 
            // tabRare
            // 
            this.tabRare.Controls.Add(this.buttonAddRare);
            this.tabRare.Controls.Add(this.checkDropsRare);
            this.tabRare.Controls.Add(this.buttonRemRare);
            this.tabRare.Controls.Add(this.dataGridRare);
            this.tabRare.Location = new System.Drawing.Point(4, 22);
            this.tabRare.Name = "tabRare";
            this.tabRare.Padding = new System.Windows.Forms.Padding(3);
            this.tabRare.Size = new System.Drawing.Size(332, 167);
            this.tabRare.TabIndex = 2;
            this.tabRare.Text = "Rare";
            this.tabRare.UseVisualStyleBackColor = true;
            // 
            // buttonAddRare
            // 
            this.buttonAddRare.Enabled = false;
            this.buttonAddRare.Location = new System.Drawing.Point(6, 109);
            this.buttonAddRare.Name = "buttonAddRare";
            this.buttonAddRare.Size = new System.Drawing.Size(96, 23);
            this.buttonAddRare.TabIndex = 7;
            this.buttonAddRare.Text = "Add";
            this.buttonAddRare.UseVisualStyleBackColor = true;
            this.buttonAddRare.Click += new System.EventHandler(this.buttonAddRare_Click);
            // 
            // checkDropsRare
            // 
            this.checkDropsRare.AutoCheck = false;
            this.checkDropsRare.AutoSize = true;
            this.checkDropsRare.Location = new System.Drawing.Point(6, 86);
            this.checkDropsRare.Name = "checkDropsRare";
            this.checkDropsRare.Size = new System.Drawing.Size(65, 17);
            this.checkDropsRare.TabIndex = 9;
            this.checkDropsRare.Text = "Enabled";
            this.checkDropsRare.UseVisualStyleBackColor = true;
            this.checkDropsRare.CheckedChanged += new System.EventHandler(this.checkDropsRare_CheckedChanged);
            this.checkDropsRare.Click += new System.EventHandler(this.checkDropsRare_Click);
            // 
            // buttonRemRare
            // 
            this.buttonRemRare.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonRemRare.Location = new System.Drawing.Point(6, 138);
            this.buttonRemRare.Name = "buttonRemRare";
            this.buttonRemRare.Size = new System.Drawing.Size(96, 23);
            this.buttonRemRare.TabIndex = 8;
            this.buttonRemRare.Text = "Remove";
            this.buttonRemRare.UseVisualStyleBackColor = true;
            this.buttonRemRare.Click += new System.EventHandler(this.buttonRemRare_Click);
            // 
            // dataGridRare
            // 
            this.dataGridRare.AllowUserToAddRows = false;
            this.dataGridRare.AllowUserToDeleteRows = false;
            this.dataGridRare.AllowUserToResizeColumns = false;
            this.dataGridRare.AllowUserToResizeRows = false;
            this.dataGridRare.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridRare.ColumnHeadersVisible = false;
            this.dataGridRare.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewComboBoxColumn2});
            this.dataGridRare.Location = new System.Drawing.Point(108, 6);
            this.dataGridRare.Name = "dataGridRare";
            this.dataGridRare.RowHeadersWidth = 16;
            this.dataGridRare.Size = new System.Drawing.Size(217, 155);
            this.dataGridRare.TabIndex = 1;
            // 
            // dataGridViewComboBoxColumn2
            // 
            this.dataGridViewComboBoxColumn2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewComboBoxColumn2.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.dataGridViewComboBoxColumn2.HeaderText = "Drops";
            this.dataGridViewComboBoxColumn2.Name = "dataGridViewComboBoxColumn2";
            // 
            // buttonSave
            // 
            this.buttonSave.Location = new System.Drawing.Point(600, 505);
            this.buttonSave.Name = "buttonSave";
            this.buttonSave.Size = new System.Drawing.Size(64, 30);
            this.buttonSave.TabIndex = 44;
            this.buttonSave.Text = "Apply";
            this.buttonSave.UseVisualStyleBackColor = true;
            this.buttonSave.Click += new System.EventHandler(this.buttonSave_Click);
            // 
            // numWeight
            // 
            this.numWeight.Location = new System.Drawing.Point(402, 280);
            this.numWeight.Maximum = new decimal(new int[] {
            9999,
            0,
            0,
            0});
            this.numWeight.Name = "numWeight";
            this.numWeight.Size = new System.Drawing.Size(65, 20);
            this.numWeight.TabIndex = 46;
            // 
            // labelWeight
            // 
            this.labelWeight.AutoSize = true;
            this.labelWeight.Location = new System.Drawing.Point(332, 282);
            this.labelWeight.Name = "labelWeight";
            this.labelWeight.Size = new System.Drawing.Size(41, 13);
            this.labelWeight.TabIndex = 47;
            this.labelWeight.Text = "Weight";
            // 
            // labelHeight
            // 
            this.labelHeight.AutoSize = true;
            this.labelHeight.Location = new System.Drawing.Point(479, 282);
            this.labelHeight.Name = "labelHeight";
            this.labelHeight.Size = new System.Drawing.Size(38, 13);
            this.labelHeight.TabIndex = 48;
            this.labelHeight.Text = "Height";
            // 
            // numHeight
            // 
            this.numHeight.Location = new System.Drawing.Point(523, 280);
            this.numHeight.Maximum = new decimal(new int[] {
            9999,
            0,
            0,
            0});
            this.numHeight.Name = "numHeight";
            this.numHeight.Size = new System.Drawing.Size(65, 20);
            this.numHeight.TabIndex = 49;
            // 
            // CharmlingsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(676, 546);
            this.Controls.Add(this.numHeight);
            this.Controls.Add(this.labelHeight);
            this.Controls.Add(this.labelWeight);
            this.Controls.Add(this.numWeight);
            this.Controls.Add(this.buttonSave);
            this.Controls.Add(this.tabsDrops);
            this.Controls.Add(this.checkPerk);
            this.Controls.Add(this.comboPerk);
            this.Controls.Add(this.groupStats);
            this.Controls.Add(this.numSecID);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.labelExp);
            this.Controls.Add(this.comboExp);
            this.Controls.Add(this.labelFamily);
            this.Controls.Add(this.comboFamily);
            this.Controls.Add(this.labelSecAttr);
            this.Controls.Add(this.comboSecAttr);
            this.Controls.Add(this.labelAttribute);
            this.Controls.Add(this.comboAttr);
            this.Controls.Add(this.numericID);
            this.Controls.Add(this.textboxName);
            this.Controls.Add(this.labelID);
            this.Controls.Add(this.labelName);
            this.Controls.Add(this.buttonRemMon);
            this.Controls.Add(this.buttonAddMon);
            this.Controls.Add(this.dataGridCharmlings);
            this.Controls.Add(this.menuMain);
            this.MainMenuStrip = this.menuMain;
            this.Name = "CharmlingsForm";
            this.Text = "CharmlingsForm";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.CharmlingsForm_FormClosed);
            this.Load += new System.EventHandler(this.CharmlingsForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridCharmlings)).EndInit();
            this.menuMain.ResumeLayout(false);
            this.menuMain.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numSecID)).EndInit();
            this.groupStats.ResumeLayout(false);
            this.groupStats.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numStatLevel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numBaseHP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numBaseAgi)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numBaseRes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numBaseMag)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numBaseDef)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numBaseStr)).EndInit();
            this.tabsDrops.ResumeLayout(false);
            this.tabCommon.ResumeLayout(false);
            this.tabCommon.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridCommon)).EndInit();
            this.tabUncommon.ResumeLayout(false);
            this.tabUncommon.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridUncommon)).EndInit();
            this.tabRare.ResumeLayout(false);
            this.tabRare.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridRare)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numWeight)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numHeight)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridCharmlings;
        private System.Windows.Forms.Button buttonAddMon;
        private System.Windows.Forms.Button buttonRemMon;
        private System.Windows.Forms.MenuStrip menuMain;
        private System.Windows.Forms.ToolStripMenuItem editToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem viewToolStripMenuItem;
        private System.Windows.Forms.Label labelName;
        private System.Windows.Forms.Label labelID;
        private System.Windows.Forms.TextBox textboxName;
        private System.Windows.Forms.NumericUpDown numericID;
        private System.Windows.Forms.Label labelAttribute;
        private System.Windows.Forms.ComboBox comboAttr;
        private System.Windows.Forms.Label labelSecAttr;
        private System.Windows.Forms.ComboBox comboSecAttr;
        private System.Windows.Forms.Label labelFamily;
        private System.Windows.Forms.ComboBox comboFamily;
        private System.Windows.Forms.Label labelExp;
        private System.Windows.Forms.ComboBox comboExp;
        private System.Windows.Forms.NumericUpDown numSecID;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupStats;
        private System.Windows.Forms.Label labelBaseMag;
        private System.Windows.Forms.Label labelBaseDef;
        private System.Windows.Forms.NumericUpDown numBaseStr;
        private System.Windows.Forms.Label labelBaseStr;
        private System.Windows.Forms.Label labelBaseRes;
        private System.Windows.Forms.Label labelBaseAgi;
        private System.Windows.Forms.Label labelBaseHP;
        private System.Windows.Forms.NumericUpDown numBaseAgi;
        private System.Windows.Forms.NumericUpDown numBaseRes;
        private System.Windows.Forms.NumericUpDown numBaseMag;
        private System.Windows.Forms.NumericUpDown numBaseDef;
        private System.Windows.Forms.NumericUpDown numBaseHP;
        private System.Windows.Forms.Label labelTotal;
        private System.Windows.Forms.NumericUpDown numStatLevel;
        private System.Windows.Forms.CheckBox checkPerk;
        private System.Windows.Forms.ComboBox comboPerk;
        private System.Windows.Forms.TabControl tabsDrops;
        private System.Windows.Forms.TabPage tabCommon;
        private System.Windows.Forms.TabPage tabUncommon;
        private System.Windows.Forms.TabPage tabRare;
        private System.Windows.Forms.Button buttonSave;
        private System.Windows.Forms.Button buttonAddComm;
        private System.Windows.Forms.DataGridView dataGridCommon;
        private System.Windows.Forms.Button buttonRemComm;
        private System.Windows.Forms.CheckBox checkDropsComm;
        private System.Windows.Forms.DataGridViewImageColumn charmlingsIcon;
        private System.Windows.Forms.DataGridViewTextBoxColumn charmlingsID;
        private System.Windows.Forms.DataGridViewTextBoxColumn charmlingsName;
        private System.Windows.Forms.TextBox textMaxHP;
        private System.Windows.Forms.TextBox textMaxAgi;
        private System.Windows.Forms.TextBox textMaxRes;
        private System.Windows.Forms.TextBox textMaxMag;
        private System.Windows.Forms.TextBox textMaxDef;
        private System.Windows.Forms.TextBox textMaxStr;
        private System.Windows.Forms.NumericUpDown numWeight;
        private System.Windows.Forms.Label labelWeight;
        private System.Windows.Forms.Label labelHeight;
        private System.Windows.Forms.NumericUpDown numHeight;
        private System.Windows.Forms.DataGridViewComboBoxColumn columnDrops;
        private System.Windows.Forms.DataGridView dataGridUncommon;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn1;
        private System.Windows.Forms.DataGridView dataGridRare;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn2;
        private System.Windows.Forms.Button buttonAddUncomm;
        private System.Windows.Forms.CheckBox checkDropsUncomm;
        private System.Windows.Forms.Button buttonRemUncomm;
        private System.Windows.Forms.Button buttonAddRare;
        private System.Windows.Forms.CheckBox checkDropsRare;
        private System.Windows.Forms.Button buttonRemRare;
    }
}