﻿namespace CharmlingsDatabaseTool
{
    partial class SkillsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SkillsForm));
            this.buttonRemSkill = new System.Windows.Forms.Button();
            this.buttonAddSkill = new System.Windows.Forms.Button();
            this.dataGridSkills = new System.Windows.Forms.DataGridView();
            this.skillID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.skillName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtSkillName = new System.Windows.Forms.TextBox();
            this.labelName = new System.Windows.Forms.Label();
            this.comboAttr = new System.Windows.Forms.ComboBox();
            this.labelAttribute = new System.Windows.Forms.Label();
            this.labelAttack = new System.Windows.Forms.Label();
            this.numAttack = new System.Windows.Forms.NumericUpDown();
            this.numCool = new System.Windows.Forms.NumericUpDown();
            this.labelCool = new System.Windows.Forms.Label();
            this.checkRanged = new System.Windows.Forms.CheckBox();
            this.comboType = new System.Windows.Forms.ComboBox();
            this.labelType = new System.Windows.Forms.Label();
            this.labelRarity = new System.Windows.Forms.Label();
            this.comboRarity = new System.Windows.Forms.ComboBox();
            this.comboExclusive = new System.Windows.Forms.ComboBox();
            this.checkExclusive = new System.Windows.Forms.CheckBox();
            this.checkPriority = new System.Windows.Forms.CheckBox();
            this.numericPriority = new System.Windows.Forms.NumericUpDown();
            this.checkEffect = new System.Windows.Forms.CheckBox();
            this.textEffect = new System.Windows.Forms.TextBox();
            this.buttonSave = new System.Windows.Forms.Button();
            this.textboxNotes = new System.Windows.Forms.TextBox();
            this.labelNotes = new System.Windows.Forms.Label();
            this.numericID = new System.Windows.Forms.NumericUpDown();
            this.labelID = new System.Windows.Forms.Label();
            this.menuMain = new System.Windows.Forms.MenuStrip();
            this.editToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.renameToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.nameToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.iDToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.viewToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.numSell = new System.Windows.Forms.NumericUpDown();
            this.labelSell = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridSkills)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numAttack)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numCool)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericPriority)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericID)).BeginInit();
            this.menuMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numSell)).BeginInit();
            this.SuspendLayout();
            // 
            // buttonRemSkill
            // 
            this.buttonRemSkill.Location = new System.Drawing.Point(194, 423);
            this.buttonRemSkill.Name = "buttonRemSkill";
            this.buttonRemSkill.Size = new System.Drawing.Size(76, 30);
            this.buttonRemSkill.TabIndex = 4;
            this.buttonRemSkill.Text = "Remove";
            this.buttonRemSkill.UseVisualStyleBackColor = true;
            this.buttonRemSkill.Click += new System.EventHandler(this.buttonRemSkill_Click);
            // 
            // buttonAddSkill
            // 
            this.buttonAddSkill.Location = new System.Drawing.Point(12, 423);
            this.buttonAddSkill.Name = "buttonAddSkill";
            this.buttonAddSkill.Size = new System.Drawing.Size(176, 30);
            this.buttonAddSkill.TabIndex = 3;
            this.buttonAddSkill.Text = "Add";
            this.buttonAddSkill.UseVisualStyleBackColor = true;
            this.buttonAddSkill.Click += new System.EventHandler(this.buttonAddSkill_Click);
            // 
            // dataGridSkills
            // 
            this.dataGridSkills.AllowUserToAddRows = false;
            this.dataGridSkills.AllowUserToDeleteRows = false;
            this.dataGridSkills.AllowUserToResizeColumns = false;
            this.dataGridSkills.AllowUserToResizeRows = false;
            this.dataGridSkills.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridSkills.ColumnHeadersVisible = false;
            this.dataGridSkills.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.skillID,
            this.skillName});
            this.dataGridSkills.Location = new System.Drawing.Point(12, 27);
            this.dataGridSkills.MultiSelect = false;
            this.dataGridSkills.Name = "dataGridSkills";
            this.dataGridSkills.ReadOnly = true;
            this.dataGridSkills.RowHeadersVisible = false;
            this.dataGridSkills.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.AutoSizeToAllHeaders;
            this.dataGridSkills.RowTemplate.Height = 28;
            this.dataGridSkills.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridSkills.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridSkills.Size = new System.Drawing.Size(258, 390);
            this.dataGridSkills.TabIndex = 2;
            this.dataGridSkills.CurrentCellChanged += new System.EventHandler(this.dataGridSkills_CurrentCellChanged);
            // 
            // skillID
            // 
            this.skillID.HeaderText = "ID";
            this.skillID.MinimumWidth = 32;
            this.skillID.Name = "skillID";
            this.skillID.ReadOnly = true;
            this.skillID.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.skillID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.skillID.Width = 32;
            // 
            // skillName
            // 
            this.skillName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.skillName.HeaderText = "Skill";
            this.skillName.Name = "skillName";
            this.skillName.ReadOnly = true;
            // 
            // txtSkillName
            // 
            this.txtSkillName.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtSkillName.Location = new System.Drawing.Point(279, 43);
            this.txtSkillName.Name = "txtSkillName";
            this.txtSkillName.Size = new System.Drawing.Size(132, 20);
            this.txtSkillName.TabIndex = 5;
            this.txtSkillName.TextChanged += new System.EventHandler(this.txtSkillName_TextChanged);
            // 
            // labelName
            // 
            this.labelName.AutoSize = true;
            this.labelName.Location = new System.Drawing.Point(276, 27);
            this.labelName.Name = "labelName";
            this.labelName.Size = new System.Drawing.Size(57, 13);
            this.labelName.TabIndex = 6;
            this.labelName.Text = "Skill Name";
            // 
            // comboAttr
            // 
            this.comboAttr.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboAttr.FormattingEnabled = true;
            this.comboAttr.Location = new System.Drawing.Point(279, 82);
            this.comboAttr.Name = "comboAttr";
            this.comboAttr.Size = new System.Drawing.Size(132, 21);
            this.comboAttr.TabIndex = 7;
            // 
            // labelAttribute
            // 
            this.labelAttribute.AutoSize = true;
            this.labelAttribute.Location = new System.Drawing.Point(276, 66);
            this.labelAttribute.Name = "labelAttribute";
            this.labelAttribute.Size = new System.Drawing.Size(46, 13);
            this.labelAttribute.TabIndex = 8;
            this.labelAttribute.Text = "Attribute";
            // 
            // labelAttack
            // 
            this.labelAttack.AutoSize = true;
            this.labelAttack.Location = new System.Drawing.Point(276, 146);
            this.labelAttack.Name = "labelAttack";
            this.labelAttack.Size = new System.Drawing.Size(38, 13);
            this.labelAttack.TabIndex = 9;
            this.labelAttack.Text = "Attack";
            // 
            // numAttack
            // 
            this.numAttack.Location = new System.Drawing.Point(279, 162);
            this.numAttack.Name = "numAttack";
            this.numAttack.Size = new System.Drawing.Size(113, 20);
            this.numAttack.TabIndex = 10;
            // 
            // numCool
            // 
            this.numCool.Location = new System.Drawing.Point(417, 82);
            this.numCool.Maximum = new decimal(new int[] {
            4,
            0,
            0,
            0});
            this.numCool.Name = "numCool";
            this.numCool.Size = new System.Drawing.Size(52, 20);
            this.numCool.TabIndex = 11;
            // 
            // labelCool
            // 
            this.labelCool.AutoSize = true;
            this.labelCool.Location = new System.Drawing.Point(414, 66);
            this.labelCool.Name = "labelCool";
            this.labelCool.Size = new System.Drawing.Size(54, 13);
            this.labelCool.TabIndex = 12;
            this.labelCool.Text = "Cooldown";
            // 
            // checkRanged
            // 
            this.checkRanged.AutoSize = true;
            this.checkRanged.Location = new System.Drawing.Point(405, 124);
            this.checkRanged.Name = "checkRanged";
            this.checkRanged.Size = new System.Drawing.Size(64, 17);
            this.checkRanged.TabIndex = 13;
            this.checkRanged.Text = "Ranged";
            this.checkRanged.UseVisualStyleBackColor = true;
            // 
            // comboType
            // 
            this.comboType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboType.FormattingEnabled = true;
            this.comboType.Location = new System.Drawing.Point(279, 122);
            this.comboType.Name = "comboType";
            this.comboType.Size = new System.Drawing.Size(100, 21);
            this.comboType.TabIndex = 14;
            // 
            // labelType
            // 
            this.labelType.AutoSize = true;
            this.labelType.Location = new System.Drawing.Point(276, 106);
            this.labelType.Name = "labelType";
            this.labelType.Size = new System.Drawing.Size(31, 13);
            this.labelType.TabIndex = 15;
            this.labelType.Text = "Type";
            // 
            // labelRarity
            // 
            this.labelRarity.AutoSize = true;
            this.labelRarity.Location = new System.Drawing.Point(276, 185);
            this.labelRarity.Name = "labelRarity";
            this.labelRarity.Size = new System.Drawing.Size(34, 13);
            this.labelRarity.TabIndex = 17;
            this.labelRarity.Text = "Rarity";
            // 
            // comboRarity
            // 
            this.comboRarity.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboRarity.FormattingEnabled = true;
            this.comboRarity.Location = new System.Drawing.Point(279, 201);
            this.comboRarity.Name = "comboRarity";
            this.comboRarity.Size = new System.Drawing.Size(190, 21);
            this.comboRarity.TabIndex = 16;
            // 
            // comboExclusive
            // 
            this.comboExclusive.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboExclusive.FormattingEnabled = true;
            this.comboExclusive.Location = new System.Drawing.Point(279, 252);
            this.comboExclusive.Name = "comboExclusive";
            this.comboExclusive.Size = new System.Drawing.Size(190, 21);
            this.comboExclusive.TabIndex = 18;
            // 
            // checkExclusive
            // 
            this.checkExclusive.AutoSize = true;
            this.checkExclusive.Location = new System.Drawing.Point(279, 229);
            this.checkExclusive.Name = "checkExclusive";
            this.checkExclusive.Size = new System.Drawing.Size(71, 17);
            this.checkExclusive.TabIndex = 20;
            this.checkExclusive.Text = "Exclusive";
            this.checkExclusive.UseVisualStyleBackColor = true;
            this.checkExclusive.CheckedChanged += new System.EventHandler(this.checkExclusive_CheckedChanged);
            // 
            // checkPriority
            // 
            this.checkPriority.AutoSize = true;
            this.checkPriority.Location = new System.Drawing.Point(279, 279);
            this.checkPriority.Name = "checkPriority";
            this.checkPriority.Size = new System.Drawing.Size(57, 17);
            this.checkPriority.TabIndex = 21;
            this.checkPriority.Text = "Priority";
            this.checkPriority.UseVisualStyleBackColor = true;
            this.checkPriority.CheckedChanged += new System.EventHandler(this.checkPriority_CheckedChanged);
            // 
            // numericPriority
            // 
            this.numericPriority.Location = new System.Drawing.Point(279, 302);
            this.numericPriority.Maximum = new decimal(new int[] {
            4,
            0,
            0,
            0});
            this.numericPriority.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericPriority.Name = "numericPriority";
            this.numericPriority.Size = new System.Drawing.Size(52, 20);
            this.numericPriority.TabIndex = 22;
            this.numericPriority.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // checkEffect
            // 
            this.checkEffect.AutoSize = true;
            this.checkEffect.Location = new System.Drawing.Point(337, 279);
            this.checkEffect.Name = "checkEffect";
            this.checkEffect.Size = new System.Drawing.Size(76, 17);
            this.checkEffect.TabIndex = 23;
            this.checkEffect.Text = "Skill Effect";
            this.checkEffect.UseVisualStyleBackColor = true;
            this.checkEffect.CheckedChanged += new System.EventHandler(this.checkEffect_CheckedChanged);
            // 
            // textEffect
            // 
            this.textEffect.Location = new System.Drawing.Point(337, 302);
            this.textEffect.Name = "textEffect";
            this.textEffect.Size = new System.Drawing.Size(132, 20);
            this.textEffect.TabIndex = 24;
            // 
            // buttonSave
            // 
            this.buttonSave.Location = new System.Drawing.Point(405, 423);
            this.buttonSave.Name = "buttonSave";
            this.buttonSave.Size = new System.Drawing.Size(64, 30);
            this.buttonSave.TabIndex = 25;
            this.buttonSave.Text = "Apply";
            this.buttonSave.UseVisualStyleBackColor = true;
            this.buttonSave.Click += new System.EventHandler(this.buttonSave_Click);
            // 
            // textboxNotes
            // 
            this.textboxNotes.Location = new System.Drawing.Point(279, 341);
            this.textboxNotes.Multiline = true;
            this.textboxNotes.Name = "textboxNotes";
            this.textboxNotes.Size = new System.Drawing.Size(190, 76);
            this.textboxNotes.TabIndex = 26;
            // 
            // labelNotes
            // 
            this.labelNotes.AutoSize = true;
            this.labelNotes.Location = new System.Drawing.Point(276, 325);
            this.labelNotes.Name = "labelNotes";
            this.labelNotes.Size = new System.Drawing.Size(35, 13);
            this.labelNotes.TabIndex = 27;
            this.labelNotes.Text = "Notes";
            // 
            // numericID
            // 
            this.numericID.Location = new System.Drawing.Point(417, 43);
            this.numericID.Maximum = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.numericID.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericID.Name = "numericID";
            this.numericID.Size = new System.Drawing.Size(52, 20);
            this.numericID.TabIndex = 28;
            this.numericID.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericID.ValueChanged += new System.EventHandler(this.numericID_ValueChanged);
            this.numericID.Leave += new System.EventHandler(this.numericID_Leave);
            // 
            // labelID
            // 
            this.labelID.AutoSize = true;
            this.labelID.Location = new System.Drawing.Point(414, 27);
            this.labelID.Name = "labelID";
            this.labelID.Size = new System.Drawing.Size(18, 13);
            this.labelID.TabIndex = 29;
            this.labelID.Text = "ID";
            // 
            // menuMain
            // 
            this.menuMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.editToolStripMenuItem,
            this.viewToolStripMenuItem});
            this.menuMain.Location = new System.Drawing.Point(0, 0);
            this.menuMain.Name = "menuMain";
            this.menuMain.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.menuMain.Size = new System.Drawing.Size(481, 24);
            this.menuMain.TabIndex = 30;
            this.menuMain.Text = "menuStrip1";
            // 
            // editToolStripMenuItem
            // 
            this.editToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.renameToolStripMenuItem});
            this.editToolStripMenuItem.Name = "editToolStripMenuItem";
            this.editToolStripMenuItem.Size = new System.Drawing.Size(39, 20);
            this.editToolStripMenuItem.Text = "Edit";
            // 
            // renameToolStripMenuItem
            // 
            this.renameToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.nameToolStripMenuItem,
            this.iDToolStripMenuItem});
            this.renameToolStripMenuItem.Name = "renameToolStripMenuItem";
            this.renameToolStripMenuItem.Size = new System.Drawing.Size(117, 22);
            this.renameToolStripMenuItem.Text = "Rename";
            // 
            // nameToolStripMenuItem
            // 
            this.nameToolStripMenuItem.Name = "nameToolStripMenuItem";
            this.nameToolStripMenuItem.Size = new System.Drawing.Size(106, 22);
            this.nameToolStripMenuItem.Text = "Name";
            // 
            // iDToolStripMenuItem
            // 
            this.iDToolStripMenuItem.Name = "iDToolStripMenuItem";
            this.iDToolStripMenuItem.Size = new System.Drawing.Size(106, 22);
            this.iDToolStripMenuItem.Text = "ID";
            // 
            // viewToolStripMenuItem
            // 
            this.viewToolStripMenuItem.Name = "viewToolStripMenuItem";
            this.viewToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.viewToolStripMenuItem.Text = "View";
            // 
            // numSell
            // 
            this.numSell.Location = new System.Drawing.Point(398, 162);
            this.numSell.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            -2147483648});
            this.numSell.Name = "numSell";
            this.numSell.Size = new System.Drawing.Size(71, 20);
            this.numSell.TabIndex = 31;
            // 
            // labelSell
            // 
            this.labelSell.AutoSize = true;
            this.labelSell.Location = new System.Drawing.Point(395, 146);
            this.labelSell.Name = "labelSell";
            this.labelSell.Size = new System.Drawing.Size(51, 13);
            this.labelSell.TabIndex = 32;
            this.labelSell.Text = "Sell Price";
            // 
            // SkillsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(481, 463);
            this.Controls.Add(this.labelSell);
            this.Controls.Add(this.numSell);
            this.Controls.Add(this.labelID);
            this.Controls.Add(this.numericID);
            this.Controls.Add(this.labelNotes);
            this.Controls.Add(this.textboxNotes);
            this.Controls.Add(this.buttonSave);
            this.Controls.Add(this.textEffect);
            this.Controls.Add(this.checkEffect);
            this.Controls.Add(this.numericPriority);
            this.Controls.Add(this.checkPriority);
            this.Controls.Add(this.checkExclusive);
            this.Controls.Add(this.comboExclusive);
            this.Controls.Add(this.labelRarity);
            this.Controls.Add(this.comboRarity);
            this.Controls.Add(this.labelType);
            this.Controls.Add(this.comboType);
            this.Controls.Add(this.checkRanged);
            this.Controls.Add(this.labelCool);
            this.Controls.Add(this.numCool);
            this.Controls.Add(this.numAttack);
            this.Controls.Add(this.labelAttack);
            this.Controls.Add(this.labelAttribute);
            this.Controls.Add(this.comboAttr);
            this.Controls.Add(this.labelName);
            this.Controls.Add(this.txtSkillName);
            this.Controls.Add(this.buttonRemSkill);
            this.Controls.Add(this.buttonAddSkill);
            this.Controls.Add(this.dataGridSkills);
            this.Controls.Add(this.menuMain);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuMain;
            this.Name = "SkillsForm";
            this.Text = "Skills";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.SkillsForm_FormClosed);
            this.Load += new System.EventHandler(this.SkillsForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridSkills)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numAttack)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numCool)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericPriority)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericID)).EndInit();
            this.menuMain.ResumeLayout(false);
            this.menuMain.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numSell)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonRemSkill;
        private System.Windows.Forms.Button buttonAddSkill;
        private System.Windows.Forms.DataGridView dataGridSkills;
        private System.Windows.Forms.TextBox txtSkillName;
        private System.Windows.Forms.Label labelName;
        private System.Windows.Forms.ComboBox comboAttr;
        private System.Windows.Forms.Label labelAttribute;
        private System.Windows.Forms.Label labelAttack;
        private System.Windows.Forms.NumericUpDown numAttack;
        private System.Windows.Forms.NumericUpDown numCool;
        private System.Windows.Forms.Label labelCool;
        private System.Windows.Forms.CheckBox checkRanged;
        private System.Windows.Forms.ComboBox comboType;
        private System.Windows.Forms.Label labelType;
        private System.Windows.Forms.Label labelRarity;
        private System.Windows.Forms.ComboBox comboRarity;
        private System.Windows.Forms.ComboBox comboExclusive;
        private System.Windows.Forms.CheckBox checkExclusive;
        private System.Windows.Forms.CheckBox checkPriority;
        private System.Windows.Forms.NumericUpDown numericPriority;
        private System.Windows.Forms.CheckBox checkEffect;
        private System.Windows.Forms.TextBox textEffect;
        private System.Windows.Forms.Button buttonSave;
        private System.Windows.Forms.TextBox textboxNotes;
        private System.Windows.Forms.Label labelNotes;
        private System.Windows.Forms.NumericUpDown numericID;
        private System.Windows.Forms.Label labelID;
        private System.Windows.Forms.DataGridViewTextBoxColumn skillID;
        private System.Windows.Forms.DataGridViewTextBoxColumn skillName;
        private System.Windows.Forms.MenuStrip menuMain;
        private System.Windows.Forms.ToolStripMenuItem editToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem renameToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem nameToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem iDToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem viewToolStripMenuItem;
        private System.Windows.Forms.NumericUpDown numSell;
        private System.Windows.Forms.Label labelSell;
    }
}