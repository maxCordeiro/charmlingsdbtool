﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;

namespace CharmlingsDatabaseTool
{
    public partial class SkillsForm : Form
    {
        formMain form;
        XmlDocument db = Data.tempDB;
        XmlNodeList dblist;
        XmlElement currentElement;

        string currentName;
        int currentID;
        int lastIndex = 0;

        bool reloadingGrid = false;

        public SkillsForm(formMain _form)
        {
            InitializeComponent();
            form = _form;
        }

        private void SkillsForm_Load(object sender, EventArgs e)
        {
            comboAttr.DataSource = Enum.GetValues(typeof(GameInfo.MonsterType));
            comboType.DataSource = Enum.GetValues(typeof(GameInfo.SkillType));
            comboRarity.DataSource = Enum.GetValues(typeof(GameInfo.Rarity));
            comboExclusive.DataSource = Data.dictMons.Keys.ToArray();

            XmlNode root = db.DocumentElement;
            XmlNode skillsNode = root.SelectSingleNode("Skills");
            dblist = skillsNode.ChildNodes;

            UpdateGrid();
        }

        void LoadData(XmlElement _element)
        {
            lastIndex = dataGridSkills.CurrentCell.RowIndex;

            txtSkillName.Text = _element.GetAttribute("id");
            numericID.Value = Data.dictSkills[_element.GetAttribute("id")];

            currentName = txtSkillName.Text;
            currentID = (int)numericID.Value;

            comboAttr.SelectedItem = Enum.Parse(typeof(GameInfo.MonsterType), _element.SelectSingleNode("montype").InnerText);
            comboType.SelectedItem = Enum.Parse(typeof(GameInfo.SkillType), _element.SelectSingleNode("skilltype").InnerText);
            comboRarity.SelectedItem = Enum.Parse(typeof(GameInfo.Rarity), _element.SelectSingleNode("skillRarity").InnerText);
            numAttack.Value = int.Parse(_element.SelectSingleNode("attack").InnerText);
            numCool.Value = int.Parse(_element.SelectSingleNode("cooldown").InnerText);
            checkRanged.Checked = bool.Parse(_element.SelectSingleNode("ranged").InnerText);
            checkExclusive.Checked = bool.Parse(_element.SelectSingleNode("hasexclusive").InnerText);
            checkEffect.Checked = bool.Parse(_element.SelectSingleNode("haseffect").InnerText);
            checkPriority.Checked = _element.SelectSingleNode("priority").InnerText != "-1";
            numSell.Value = int.Parse(_element.SelectSingleNode("sellprice").InnerText);

            if (checkPriority.Checked)
            {
                numericPriority.Value = int.Parse(_element.SelectSingleNode("priority").InnerText);
            } else
            {
                numericPriority.Value = 1;
                numericPriority.Enabled = false;
            }

            if (checkExclusive.Checked)
            {
                comboExclusive.SelectedItem = _element.SelectSingleNode("exclusive").InnerText;
            } else
            {
                comboExclusive.SelectedItem = "GUBBERS";
                comboExclusive.Enabled = false;
            }

            if (checkEffect.Checked)
            {
                textEffect.Text = _element.SelectSingleNode("effectid").InnerText;
            } else
            {
                textEffect.Text = "";
                textEffect.Enabled = false;
            }

            if (Data.dictDevSkills.ContainsKey(currentName))
            {
                textboxNotes.Text = Data.dictDevSkills[currentName];
            } else
            {
                textboxNotes.Text = "";
            }
        }

        void SaveData(XmlElement _element)
        {
            if (currentName != txtSkillName.Text || currentID != (int)numericID.Value)
            {
                Data.dictSkills.Remove(currentName);
                Data.dictSkills.Add(txtSkillName.Text, (int)numericID.Value);
            }
            
            _element.SetAttribute("id", txtSkillName.Text);
            _element.SelectSingleNode("montype").InnerText = comboAttr.SelectedItem.ToString();
            _element.SelectSingleNode("skilltype").InnerText = comboType.SelectedItem.ToString();
            _element.SelectSingleNode("skillRarity").InnerText = comboRarity.SelectedItem.ToString();
            _element.SelectSingleNode("attack").InnerText = numAttack.Value.ToString();
            _element.SelectSingleNode("cooldown").InnerText = numCool.Value.ToString();
            _element.SelectSingleNode("ranged").InnerText = checkRanged.Checked.ToString().ToLower();
            _element.SelectSingleNode("hasexclusive").InnerText = checkExclusive.Checked.ToString().ToLower();
            _element.SelectSingleNode("exclusive").InnerText = checkExclusive.Checked ? comboExclusive.SelectedItem.ToString() : "GUBBERS";
            _element.SelectSingleNode("haseffect").InnerText = checkEffect.Checked.ToString().ToLower();
            
            if (checkEffect.Checked)
            {
                _element.SelectSingleNode("effectid").InnerText = textEffect.Text;
            } else
            {
                _element.SelectSingleNode("effectid").InnerText = "";
                ((XmlElement)_element.SelectSingleNode("effectid")).IsEmpty = false;
            }

            _element.SelectSingleNode("priority").InnerText = checkPriority.Checked ? numericPriority.Value.ToString() : "-1";
            _element.SelectSingleNode("sellprice").InnerText = numSell.Value.ToString();

            db.Save(Path.Combine(formMain.backupPath, "db.xml"));

            if (textboxNotes.Text != "")
            {
                if (Data.dictDevSkills.ContainsKey(currentName))
                {
                    Data.dictDevSkills[currentName] = textboxNotes.Text;
                } else
                {
                    Data.dictDevSkills.Add(currentName, textboxNotes.Text);
                }
            }

            Utility util = new Utility();
            util.WriteEnum(0);
            util.WriteNotesToText(Properties.Settings.Default.GamePath, 0);
            util.LoadEnumsToDict(Properties.Settings.Default.GamePath);

            UpdateGrid();
        }

        public void UpdateGrid()
        {
            reloadingGrid = true;
            dataGridSkills.Rows.Clear();

            foreach (XmlNode node in dblist)
            {
                XmlElement element = (XmlElement)node;

                string name = element.GetAttribute("id") + " - " + string.Format("({0})/{1}", element.SelectSingleNode("cooldown").InnerText, element.SelectSingleNode("attack").InnerText);

                dataGridSkills.Rows.Add(Data.dictSkills[element.GetAttribute("id")], name);
            }

            reloadingGrid = false;

            dataGridSkills.CurrentCell = dataGridSkills.Rows[lastIndex].Cells[1];
        }

        private void checkExclusive_CheckedChanged(object sender, EventArgs e)
        {
            comboExclusive.Enabled = checkExclusive.Checked;

            if (!comboExclusive.Enabled)
            {
                comboExclusive.SelectedItem = "GUBBERS";
            }
        }

        private void checkPriority_CheckedChanged(object sender, EventArgs e)
        {
            numericPriority.Enabled = checkPriority.Checked;

            if (!numericPriority.Enabled)
            {
                numericPriority.Value = 1;
            }
        }

        private void checkEffect_CheckedChanged(object sender, EventArgs e)
        {
            textEffect.Enabled = checkEffect.Checked;

            if (!textEffect.Enabled)
            {
                textEffect.Text = "";
            }
        }

        private void numericID_Leave(object sender, EventArgs e)
        {
            
        }

        private void numericID_ValueChanged(object sender, EventArgs e)
        {
            bool idExists = false;

            foreach (var entry in Data.dictSkills)
            {
                if (entry.Key != dataGridSkills.CurrentCell.Value.ToString() && entry.Value == (int)numericID.Value)
                {
                    idExists = true;
                    break;
                }
            }

            if (idExists)
            {
                numericID.BackColor = Color.Red;
                buttonSave.Enabled = false;
            }
            else
            {
                numericID.BackColor = Color.White;
                buttonSave.Enabled = true;
            }
        }

        private void txtSkillName_TextChanged(object sender, EventArgs e)
        {
            bool idExists = false;

            foreach (var entry in Data.dictSkills)
            {
                if (entry.Key != dataGridSkills.CurrentCell.Value.ToString() && entry.Key == txtSkillName.Text)
                {
                    idExists = true;
                    break;
                }
            }

            if (idExists)
            {
                txtSkillName.BackColor = Color.Red;
                buttonSave.Enabled = false;
            }
            else
            {
                txtSkillName.BackColor = Color.White;
                buttonSave.Enabled = true;
            }
        }

        private void buttonSave_Click(object sender, EventArgs e)
        {
            SaveData(currentElement);
        }

        private void buttonAddSkill_Click(object sender, EventArgs e)
        {
            if (Data.dictSkills.Values.Contains(0)) return;

            XmlNode root = db.DocumentElement;
            XmlNode monsterElement = root.SelectSingleNode("Skills");

            XmlElement newSkill = db.CreateElement("Skill");
            currentName = "NEWSKILL";
            currentID = 100;
            newSkill.SetAttribute("id", currentName);

            XmlElement attack = db.CreateElement("attack");
            XmlElement cooldown = db.CreateElement("cooldown");
            XmlElement ranged = db.CreateElement("ranged");
            XmlElement skilltype = db.CreateElement("skilltype");
            XmlElement exclusive = db.CreateElement("exclusive");
            XmlElement montype = db.CreateElement("montype");
            XmlElement effectid = db.CreateElement("effectid");
            XmlElement haseffect = db.CreateElement("haseffect");
            XmlElement hasexclusive = db.CreateElement("hasexclusive");
            XmlElement priority = db.CreateElement("priority");
            XmlElement skillrarity = db.CreateElement("skillRarity");
            XmlElement sellprice = db.CreateElement("sellprice");
            attack.InnerText = "0";
            cooldown.InnerText = "1";
            ranged.InnerText = "false";
            skilltype.InnerText = "SPECIAL";
            exclusive.InnerText = "GUBBERS";
            montype.InnerText = "WATER";
            haseffect.InnerText = "false";
            hasexclusive.InnerText = "false";
            priority.InnerText = "-1";
            skillrarity.InnerText = "COMMON";
            sellprice.InnerText = "0";

            newSkill.AppendChild(attack);
            newSkill.AppendChild(cooldown);
            newSkill.AppendChild(ranged);
            newSkill.AppendChild(skilltype);
            newSkill.AppendChild(exclusive);
            newSkill.AppendChild(montype);
            newSkill.AppendChild(haseffect);
            newSkill.AppendChild(cooldown);
            newSkill.AppendChild(hasexclusive);
            newSkill.AppendChild(priority);
            newSkill.AppendChild(skillrarity);
            newSkill.AppendChild(cooldown);
            newSkill.AppendChild(sellprice);
            newSkill.AppendChild(effectid);

            Data.dictSkills.Add(currentName, currentID);
            
            Utility util = new Utility();
            util.WriteEnum(0);
            util.LoadEnumsToDict(Properties.Settings.Default.GamePath);

            monsterElement.AppendChild(newSkill);
            
            db.Save(Path.Combine(formMain.backupPath, "db.xml"));
            
            dblist = monsterElement.ChildNodes;
            currentElement = newSkill;

            dataGridSkills.Rows.Add(currentID, currentName);
            dataGridSkills.CurrentCell = dataGridSkills.Rows[dataGridSkills.Rows.Count - 1].Cells[1];
            
        }

        private void SkillsForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            form.Enabled = true;
        }

        private void dataGridSkills_CurrentCellChanged(object sender, EventArgs e)
        {
            if (reloadingGrid) return;

            foreach (XmlNode node in dblist)
            {
                currentElement = (XmlElement)node;

                if (currentElement.GetAttribute("id") == dataGridSkills.CurrentRow.Cells[1].Value.ToString())
                {
                    LoadData(currentElement);
                    break;
                }
            }
        }

        private void buttonRemSkill_Click(object sender, EventArgs e)
        {
            var confirmResult = MessageBox.Show("Are you sure you want to delete this Skill?", "", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);

            if (confirmResult == DialogResult.Yes)
            {
                XmlNode root = db.DocumentElement;
                XmlNode monsterElement = root.SelectSingleNode("Skills");

                monsterElement.RemoveChild(currentElement);
                Data.dictSkills.Remove(currentName);
                
                db.Save(Path.Combine(formMain.backupPath, "db.xml"));

                Utility util = new Utility();
                util.WriteEnum(0);
                util.LoadEnumsToDict(Properties.Settings.Default.GamePath);

                lastIndex = 0;

                UpdateGrid();
            }
        }
    }
}
