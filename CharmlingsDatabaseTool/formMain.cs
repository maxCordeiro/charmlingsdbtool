﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Diagnostics;

namespace CharmlingsDatabaseTool
{
    public partial class formMain : Form
    {
        public static readonly string backupPath = Path.Combine(Application.StartupPath, "backup");
        
        Utility util;

        public formMain()
        {
            InitializeComponent();
        }

        private void buttonCharmlings_Click(object sender, EventArgs e)
        {
            CharmlingsForm ch = new CharmlingsForm(this);
            ch.Show();
            this.Enabled = false;
        }

        private void openDirectoryToolStripMenuItem_Click(object sender, EventArgs e)
        {
            dbDialog.Filter = "XML Database|*.xml";
            
            if (dbDialog.ShowDialog() == DialogResult.OK)
            {
                Properties.Settings.Default.GamePath = Directory.GetParent(Directory.GetParent(Path.GetDirectoryName(dbDialog.FileName)).ToString()).ToString();

                Properties.Settings.Default.DBPath = dbDialog.FileName;
                Properties.Settings.Default.Save();
                                
                Data.dbLoaded = true;
            } else
            {
                Data.dbLoaded = false;
            }

            UpdateLoadedDB();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            if (Properties.Settings.Default.DBPath != "")
            {
                Data.dbLoaded = true;
            }
            
            UpdateLoadedDB();
        }

        public void UpdateLoadedDB()
        {
            if (Data.dbLoaded)
            {
                string enumPath = Properties.Settings.Default.GamePath + "/Assets/Scripts/Enums/";

                Directory.CreateDirectory(backupPath);

                string pathDB = Path.Combine(backupPath, Path.GetFileName(Properties.Settings.Default.DBPath));
                string pathSkills = Path.Combine(backupPath, "Skills.cs");
                string pathMons = Path.Combine(backupPath, "Monsters.cs");
                string pathPerks = Path.Combine(backupPath, "Perks.cs");
                string pathItems = Path.Combine(backupPath, "Items.cs");

                File.Copy(Properties.Settings.Default.DBPath, pathDB, true);
                File.Copy(enumPath + "Skills.cs", pathSkills, true);
                File.Copy(enumPath + "Monsters.cs", pathMons, true);
                File.Copy(enumPath + "Perks.cs", pathPerks, true);
                File.Copy(enumPath + "Items.cs", pathItems, true);

                Data.tempDB = new System.Xml.XmlDocument();
                Data.tempDB.Load(pathDB);

                Data.enumPaths[0] = pathSkills;
                Data.enumPaths[1] = pathMons;
                Data.enumPaths[2]= pathPerks;
                Data.enumPaths[3] = pathItems;
            }

            util = new Utility();
            util.LoadEnumsToDict(Properties.Settings.Default.GamePath);

            EnableButtons(Data.dbLoaded);

            labelLoaded.Text = string.Format("Database {0} loaded{1}", Data.dbLoaded ? "" : "not", Data.dbLoaded ? "! (" + Properties.Settings.Default.DBPath + ")" : "");
        }
        
        private void buttonSkills_Click(object sender, EventArgs e)
        {
            SkillsForm ch = new SkillsForm(this);
            ch.Show();
            this.Enabled = false;
        }

        void EnableButtons(bool loaded)
        {
            buttonCharmlings.Enabled = loaded;
            buttonSkills.Enabled = loaded;
        }

        private void formMain_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (Data.tempDB != null)
            {
                string docA = File.ReadAllText(Properties.Settings.Default.DBPath);
                string tempDoc = File.ReadAllText(Path.Combine(backupPath, "db.xml"));
                
                if (string.Compare(docA, tempDoc, true) == 0) return;

                var confirmResult = MessageBox.Show("You have unsaved changes. Are you sure you want to exit?", "", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);

                e.Cancel = (confirmResult == DialogResult.No);
            }
        }
    }
}
